// 1 - JS Function Parameters and Return Statement, Selection Control Structures

function shipItem(item, weight, location) {
    let shippingFee;
  
    // shipping fee based on weight
    if (weight == 3) {
        shippingFee = 150;
      } else if (weight > 3 && weight <= 5) {
        shippingFee = 280;
      } else if (weight > 5 && weight <= 8) {
        shippingFee = 340;
      } else if (weight > 8 && weight <= 10) {
        shippingFee = 410;
      } else if (weight >= 10) {
        shippingFee = 560;
    }else
      { 
        return `The allowed minimum weight is 3kg`;
    };
  
    // additional charge based on location 
    switch (location) {
      case 'local':
        break;
      case 'international':
        shippingFee += 250;
        break;
      default:
        return 'Invalid location';
    };
      return `The total shipping fee for the ${item} that will ship ${location} is P${shippingFee}`;
};
  // call the function
  console.log(shipItem('gaming chair', 8, 'international'));


// 2 - 3. JS Objects and Array Manipulation

  // Product constructor
function Product(name, price, quantity) {
  this.name = name;
  this.price = price;
  this.quantity = quantity;
};

// Customer constructor
function Customer(name) {
  this.name = name;
  this.cart = [];

  this.addToCart = function(product) {
    this.cart.push(product);
    return `${product.name} is added to cart.`;
  };

  this.removeToCart = function(product) {
    let index = this.cart.indexOf(product);

    if (index !== -1) {
      this.cart.splice(index, 1);
      return `${product.name} is removed from the cart.`;
    }else{
      return `${product.name} is not found in the cart.`;
    };
  };
};

// New product and customer
let product = new Product('nichirin sword', 1000, 1);
let customer = new Customer('Tanjiro');

// add the product to the cart
console.log(customer.addToCart(product));

// remove the product from the cart
console.log(customer.removeToCart(product));



